#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This file is part of utf8_to_licr.
#
# Copyright © 2019 Andrej Radovic <r.andrej@gmail.com>
#
# utf8_to_licr is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# utf8_to_licr is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with utf8_to_licr.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import re
import subprocess
import sys
from typing import Dict, TextIO


def get_texlive_path(filename: str) -> str:
    """Get a path to a file as recognized by texlive's kpsewhich."""
    # TODO add support for MikTex's miktex-kpsewhich.exe on Windows
    dtx_path_query = subprocess.run(
        ["kpsewhich", filename], stdout=subprocess.PIPE
    )
    return dtx_path_query.stdout.decode("utf-8").splitlines().pop()


def create_translation_table(ienc_dict: TextIO) -> Dict[int, str]:
    """Create a translation table for utf-8 → LICR."""

    pattern = re.compile(r"\DeclareUnicodeCharacter\{([0-9A-Fa-f]+)\}\{(.*)\}")

    with ienc_dict:
        match_groups = (
            pattern.search(line).groups()
            for line in ienc_dict
            if pattern.search(line)
        )

        translation_table = {
            int(codepoint, base=16): latex.replace("@tabacckludge", "")
            for codepoint, latex in match_groups
        }

        if not translation_table:
            raise RuntimeError(
                "The dictionary file provided doesn't contain any valid "
                "character definitions"
            )

        return translation_table


parser = argparse.ArgumentParser(
    description="UTF-8 to LICR (LaTeX Internal Character Representation) converter."
)

parser.add_argument(
    "infile",
    nargs="?",
    type=argparse.FileType("r", encoding="utf-8"),
    default=sys.stdin,
    help="Input file. If omitted, reads STDIN.",
)
parser.add_argument(
    "-o",
    "--outfile",
    type=argparse.FileType("w", encoding="ascii"),
    default=sys.stdout,
    help="Output file. If omitted, writes to STDOUT.",
)
parser.add_argument(
    "-d",
    "--dictfile",
    type=argparse.FileType("r", encoding="utf-8"),
    default=open(get_texlive_path("utf8ienc.dtx"), "r", encoding="utf-8"),
    help=(
        "Path of the 'utf8ienc.dtx' dictionary file. "
        "By default obtained by calling `kpsewhich utf8ienc.dtx`."
    ),
)

args = parser.parse_args()

translation_table = create_translation_table(args.dictfile)

with args.infile as infile, args.outfile as outfile:
    for line in infile:
        line: str
        outfile.write(line.translate(translation_table))
