utf8_to_licr
============

`utf8_to_licr` is a simple Python commandline utility to convert UTF-8 encoded
text to LaTeX Internal Character Representation.
Its main purpose is to aid writing LaTeX localization packages (like
`datetime-*` and `babel-*`) by allowing you to write localizations in UTF-8
(like a sane person).
You may then use this utility to generate LICR encoded strings from the UTF-8
strings you wrote.

The utility is written in a simple, Unix like fashion and you can use it with
files or `STDIN`/`STDOUT`.

Usage
-----

```
usage: utf_to_licr.py [-h] [-o OUTFILE] [-d DICTFILE] [infile]

UTF-8 to LICR (LaTeX Internal Character Representation) converter.

positional arguments:
  infile                Input file. If omitted, reads STDIN.

optional arguments:
  -h, --help            show this help message and exit
  -o OUTFILE, --outfile OUTFILE
                        Output file. If omitted, writes to STDOUT.
  -d DICTFILE, --dictfile DICTFILE
                        Path of the 'utf8ienc.dtx' dictionary file. By default
                        obtained by calling `kpsewhich utf8ienc.dtx`.
```

It's very easy to use in Vim: `'<,'>!./utf_to_licr.py` converts the selected 
text to LICR.

Examples
--------

```
raščupaću → ra\v s\v cupa\'cu
шћућурити → \cyrsh\cyrtshe\cyru\cyrtshe\cyru\cyrr\cyri\cyrt\cyri
```

How it works
------------

It's simply parses `utf8ienc.dtx` from your Texlive distribution to create a
translation table, which it uses to translate UTF-8 to LICR.
The path of `utf8ienc.dtx` is obtained using `kpsewhich` by default, but may be
provided via a commandline flag.

CTAN? PyPI?
===========

I'll upload the utility to CTAN and/or PyPI if there is interest.
